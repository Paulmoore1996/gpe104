﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astroid : MonoBehaviour {
    public float maxThrust; //sets designer thrust speed
    public float maxSpin; //designer set spin speed
    public Rigidbody2D rb2D; //designer set for Rigidbody2D
    public GameObject Player; //set player Gameobject
    public float screenTop; //Variable for top of the screen
    public float screenBottom; //public variable for the bottom of the screen
    public float screenLeft;//public variable for the left of the screen
    public float screenRight;//public variable of the right of the screen
    public GameObject target; //target public variable
    public GameObject bullet; // public variable for the bullet
    public int points; // public point number that they give.
    private GameObject Spawn; // private spawn game object
    

    // Use this for initialization
    void Start () {
        Vector2 thrust = new Vector2(Random.Range(-maxThrust, maxThrust),Random.Range(-maxThrust, maxThrust));//Creates a thrust vector to give astroids a range of speed based on designer input.
        float spin = Random.Range(-maxSpin, maxSpin);//creates a spin number based on designer input.
        Player = GameObject.FindWithTag("Player"); // sets the Player tag as Player  
        Spawn = GameObject.FindWithTag("MainCamera");//the spawn point is in the MainCamera tag.
        rb2D.AddTorque(spin);  //adds spin to the astroid
        rb2D.AddRelativeForce(thrust);//adds speed to the astroid.
        if (Player != null)
        {
            transform.position = Vector2.MoveTowards(transform.position, Player.transform.position, Time.deltaTime * maxThrust);
        }
    }
	// Update is called once per frame
	void Update () {
        if (transform.position.y > screenTop || transform.position.y < screenBottom)//creates a limit set by the designer for the top and bottom of the border.
        {
            Spawn.SendMessage("EnemyLimit", -1);//When you cross the borderlimit it will decrease the enemy limit by 1,
            Destroy(target);    // destroys the player if he goes too far above or below the screen.
        }
        if (transform.position.x > screenRight || transform.position.x < screenLeft)//creates a limit set by the designer for the left and right of the border.
        {
            Spawn.SendMessage("EnemyLimit", -1);//When you cross the borderlimit it will decrease the enemy limit by 1,
            Destroy(target); // Destroys the player if he goes too far left or right of the screen.
        }
    }
    void OnTriggerEnter2D(Collider2D gameObject)//Creates collision trigger.
    {
        if (gameObject.name == "Bullet(Clone)")//If the bullet touches the Astroid
            
        {
            Spawn.SendMessage("EnemyLimit", -1);//it will decrease the enemy limit by 1,
            Player.SendMessage("ScorePoints", points);//It will increase player points.
            Destroy(target);//It will destroy the astroid.
        }
        if(gameObject.name == "PlayerStarShip")//if it hits the player.
        {
            Player.SendMessage("lifePoint", 1);//Decrease player lives by 1.
            Player.SetActive(false);//disable the Player GameObject.
            Destroy(target);//Destroys the astroid.
        }
    }

}



