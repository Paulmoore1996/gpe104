﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SpaceShipControls : MonoBehaviour {
    public Rigidbody2D rb; //Sets the Rigidbody2D for specific stat setting in the code.
    public GameObject player; //Fetches the GameObject for the player.
    public float thrust; // sets the thrust as a public number
    public float turnThrust; // sets the turn thrust as a public number
    private float thrustInput;  //sets the value of the thrustInput as a number holder.
    private float turnInput; // sets the value of the  turnInput as a number holder.
    public GameObject bullet; // sets the bullet value public, and a game object.
    public float screenTop; //Variable for top of the screen
    public float screenBottom; //public variable for the bottom of the screen
    public float screenLeft;//public variable for the left of the screen
    public float screenRight;//public variable of the right of the screen
    public float BulletDespawn; // Number of seconds until the bullets are destroy'd.
    public float bulletForce; //Speed the bullet will travel
    public int score; //Designer can set the base score.
    public int lives;//designer can set the number of lives.
    public Text scoreText;//we can set a canvas text here.
    public Text liveText;//we can set a live text here.
    public GameObject GameOverPanel;//designer can set the game over panel
    // Use this for initialization
    void Start()
    {
        liveText.text = "Lives " + lives;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))//creates bullet instances if space is pressed
        {
            GameObject newBullet = Instantiate(bullet, transform.position, transform.rotation);//sets the game object to the bullet
            newBullet.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * bulletForce);//shoots the bullet forward.
            Destroy(newBullet, BulletDespawn);//Despawns the bullet after a designer set number.


            thrustInput = Input.GetAxis("Vertical");//Gets key input from the Axis settings.

            turnInput = Input.GetAxis("Horizontal");//Gets key input from the Axis settings.
        }

        if (transform.position.y > screenTop || transform.position.y < screenBottom)//designer sets the top and bottom of the borders.
        {
            lives--;//when crossed you will lose 1 life.
            liveText.text = "Lives " + lives;//we update live text here.
            if (lives <= 0)//if you have no lives left
            {
                GameOver();//run GameOver function
            }
            else if (lives >= 1)//if you still have at least one life.
            {
                Invoke("Respawn", 3f);//Run the Respawn function.
            }    
        }
        if (transform.position.x > screenRight || transform.position.x < screenLeft)// destroys the player if he goes too far left or right of the screen.
        {
            lives--;//when crossed you will lose 1 life.
            liveText.text = "Lives " + lives;//we update live text here.
            if (lives <= 0)//if you have no lives left
            {

                GameOver();//run GameOver function
            }
            else if (lives >= 1)//if you still have at least one life.
            {
                Invoke("Respawn", 3f);//Run the Respawn function.
            }
        }
        rb.AddRelativeForce(Vector2.up * thrustInput);//Adds force to the movement based on key press.
        rb.AddTorque(-turnInput);//adds rotation based on key press.

    }
    void ScorePoints(int pointsToAdd)//Creates a function for scoring points.
    {
        score += pointsToAdd; // we add to the score the pointsToAdd number.
        scoreText.text = "Score " + score;//update score text.
    }
    void lifePoint(int pointsToSubtract)//create a way to subtract lives.
    {
        lives -= pointsToSubtract;//take away from lives what pointsToSubtract is.
        liveText.text = "Lives " + lives;//updates the lives text.
        if (lives <= 0)//if lives hit zero
        {
            GameOver();//run gameOver.
        }
        else if (lives >= 1)//if lives is more than one
        {
            Invoke("Respawn", 3f);//runs the respawn after 3 seconds.
        }
    }
    void Respawn()//set the respawn function
    {
        transform.position = Vector2.zero;//reset player position.
        rb.velocity = Vector2.zero;//reset player speed.
        player.SetActive(true);//turn the player back on.
    }
    public void PlayAgain()//creates the play again function
    {
        SceneManager.LoadScene(0);//reloads the game
    }
    public void Endgame()//creates endGame function
    {
        Application.Quit();//exits the game.
    }
    void GameOver()//creates a gameover function
    {
        transform.position = Vector2.zero;//resets the player positon
        rb.velocity = Vector2.zero;//resets the player speed
        CancelInvoke();//stops all invokes.
        GameOverPanel.SetActive(true);//turns on the end game panel.
    }
}