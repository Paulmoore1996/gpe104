﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {
    public GameObject ThirdAstroid;
    public GameObject AlienShip;
    public float AstroidSpawn;
    public float AlienSpawn;
    private float astroidLimit;
    public GameObject Player;
    public Transform[] spawnPoints;
	// Use this for initialization
	void Start () {
        Player = GameObject.FindWithTag("Player");
        InvokeRepeating("Spawn", 0.01f, 0.01f);
    }
	// Update is called once per frame
	void Update () {
    }
    void EnemyLimit(int Increase)
    {
        astroidLimit += Increase;
    }
    void Spawn()
    {
        if (Random.value < AstroidSpawn)
        {
            if (astroidLimit < 3)
            {
                int spawnPointIndex = Random.Range(0, spawnPoints.Length);
                Instantiate(ThirdAstroid, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
                astroidLimit++;
            }
        }
        if (Random.value < AlienSpawn)
        {
            if (astroidLimit < 3)
            {
                int spawnPointIndex = Random.Range(0, spawnPoints.Length);
                Instantiate(AlienShip, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
                astroidLimit++;
            }
        }
    }
}
