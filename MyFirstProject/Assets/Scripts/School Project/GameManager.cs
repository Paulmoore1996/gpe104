﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public static GameManager instance;
    public int score = 0;
    void Awake() {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        } else
        {
            Destroy(this.gameObject);
            Debug.Log("warnings: A second game manager was detected and destroyed.");
        }
    }
    public void GameOver()
    {
        Application.Quit();
    }
}
