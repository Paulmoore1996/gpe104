﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : MonoBehaviour {
    public float maxThrust; //sets designer thrust speed
    public float maxSpin; //designer set spin speed
    public Rigidbody2D rb2D; //designer set for Rigidbody2D
    public int points;//designer can set point increase.
    public float screenTop; //Variable for top of the screen
    public float screenBottom; //public variable for the bottom of the screen
    public float screenLeft;//public variable for the left of the screen
    public float screenRight;//public variable of the right of the screen
    public GameObject target;//designer can set the target variable
    public GameObject bullet;//designer can set the bullet variable
    private GameObject Spawn;//creates spawns
    GameObject Player; // Game object variable for the player
    // Use this for initialization
    void Start () {
        float spin = Random.Range(-maxSpin, maxSpin);//creates a random spin number
        Spawn = GameObject.FindWithTag("MainCamera");//finds the spawns in the MainCamera.
        Player = GameObject.FindWithTag("Player"); // sets the Playertag as Player
        rb2D.AddRelativeForce(-transform.position * maxThrust); //sends the object to player position.
        rb2D.AddTorque(spin);//adds spin.
    }	
	// Update is called once per frame
	void Update () { 
        if (transform.position.y > screenTop || transform.position.y < screenBottom)//creates a limit set by the designer for the top and bottom of the border.
        {
            Spawn.SendMessage("EnemyLimit", -1);//When you cross the borderlimit it will decrease the enemy limit by 1,
            Destroy(target);    // destroys the player if he goes too far above or below the screen.
        }
        if (transform.position.x > screenRight || transform.position.x < screenLeft)//creates a limit set by the designer for the left and right of the border.
        {
            Spawn.SendMessage("EnemyLimit", -1);//When you cross the borderlimit it will decrease the enemy limit by 1
            Destroy(target); // Destroys the player if he goes too far left or right of the screen.
        }
        if (Player == null)//if player does not exist
        {
            Destroy(target);//destory the Enemy ship
        }
        if (Player != null)//if the player exists
        {
            transform.position = Vector2.MoveTowards(transform.position, Player.transform.position, Time.deltaTime * maxThrust);//go to the player.
        }
    }
    void OnTriggerEnter2D(Collider2D gameObject)//if something enters the enemy shit
    {
        if (gameObject.name == "Bullet(Clone)")//if a bullet touches the enemy ship

        {
            Spawn.SendMessage("EnemyLimit", -1);// decrease the enemy limit by 1
            Player.SendMessage("ScorePoints", points);//increase score points
            Destroy(target);//destroy enemy ship
        }
        if (gameObject.name == "PlayerStarShip")//if it touches teh player
        {
            Player.SendMessage("lifePoint", 1);//decrease life points by one.
            Player.SetActive(false);//set the player inactive
            Destroy(target);//destroy enemy shit.
        }
    }
}
