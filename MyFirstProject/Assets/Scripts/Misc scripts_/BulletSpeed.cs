﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpeed : MonoBehaviour {
    public float speed = 10f;
    public Rigidbody2D rigid2d;
	// Use this for initialization
	void Start () {
        rigid2d = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        rigid2d.velocity = Vector3.forward * speed;
        

	}
}
