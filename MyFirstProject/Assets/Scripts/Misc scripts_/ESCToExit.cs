﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESCToExit : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Escape)) { // if esc key is pressed
            Application.Quit(); // exit application
        };
	}
}
