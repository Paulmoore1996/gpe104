﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PKey : MonoBehaviour {
    private Movement Rend; // Makes rend a variable
	// Use this for initialization
	void Start () {
        Rend = GetComponent<Movement>(); // sets Rend as the Renderer Component
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.P)) // if P is pressed
        {
            if (Rend.enabled == false) // If the script for moving the Beach Ball is inactive
            {
                Rend.enabled = true; // activate the beach ball movement
            }
            else if (Rend.enabled == true) // If the script for moving the Beach Ball is active
            {
                Rend.enabled = false; // deactivate the beach ball movement
            }
        };
    }
}
