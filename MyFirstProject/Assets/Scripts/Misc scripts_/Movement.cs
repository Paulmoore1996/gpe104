﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    Rigidbody2D m_Rigidbody;
    public GameObject bullet;
    private Transform tf;//makes the transform value private, and a variable.
    public float speed = 0.25f; // Publicly sets the speed to 0.25
    public float rotateSpeed = 0.25f;
    public float bulletSpeed = 10;
    // Use this for initialization
    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        tf = GetComponent<Transform>();//gets the pbject as the tf value
    }

    // Update is called once per frame
    void Update()
    {
     
        {
            //if space right mouse key is pressed
            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameObject newObject = Instantiate(bullet, tf.position, tf.rotation) as GameObject;//sets the game object to the bullet
                
                newObject.SetActive(true);
                Destroy(newObject, 100);
                }
            }
            //Fire the bullet.

            if (Input.GetKey(KeyCode.LeftArrow))//if left arrow is pressed
            {
            tf.Rotate(Vector3.forward, rotateSpeed); //rotates the sprite left
            };
            if (Input.GetKey(KeyCode.RightArrow))//if right arrow is pressed
            {
            tf.Rotate(Vector3.forward, -rotateSpeed);//rotates the sprite right
            };
            if (Input.GetKey(KeyCode.UpArrow))//if up arrow is pressed
            {
                 tf.position = tf.position + (Vector3.up * speed);
            };
            if (Input.GetKey(KeyCode.DownArrow))//if down arrow is pressed
            {
                 tf.position = tf.position + (Vector3.down * speed);//moves the sprite down per frame
            };
        }
    }

