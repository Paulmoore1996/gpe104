﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToExit : MonoBehaviour {
    //when enter, exit application
    void OnTriggerEnter2D(Collider2D gameObject)
    {
        if (gameObject.name == "PlayerStarShip") { //Makes it so it only exits if the Player enters the Exit.
            Application.Quit();
        }
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}
}
