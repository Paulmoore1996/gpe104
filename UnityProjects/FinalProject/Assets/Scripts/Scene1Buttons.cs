﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene1Buttons : MonoBehaviour {

    //scene 1 variables
    public GameObject MainMenu;//Main menu game object
    public GameObject AboutMenu;//about menu game object
    public GameObject ControlMenu;//control menu game object
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    //button functions for first scene
    public void QuitGame()//function quit game
    {
        Application.Quit();//exits the application
    }
    public void MainMenuToAboutMenu()//going from main menu to about menu
    {
        MainMenu.SetActive(false);//turn main menu off
        AboutMenu.SetActive(true);//turn about menu on
    }
    public void AboutMenutToMainMenu()//go from about menu to main menu
    {
        AboutMenu.SetActive(false);//turn about menu off
        MainMenu.SetActive(true);//turn main menu on
    }
    public void MainMenuToControlMenu()//go from main menu to control menu
    {
        MainMenu.SetActive(false);//turn main menu off
        ControlMenu.SetActive(true);//turn control menu on
    }
    public void ControlMenuToMainMenu()//go from control menu to mian menu
    {
        ControlMenu.SetActive(false);//turn control menu off
        MainMenu.SetActive(true);//turn main menu on
    }
    public void StartGame()//go to the next scene
    {
        SceneManager.LoadScene(1);//loads the "Game" scene.
    }
}
