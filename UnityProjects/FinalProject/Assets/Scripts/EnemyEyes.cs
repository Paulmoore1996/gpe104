﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyEyes : MonoBehaviour {
    public string Side;
    public GameObject Player;
    private Animator animator;
    public Animator playerAnimation;
    public float speed;
    public PlayerMovement PlayerMovement;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        Player = GameObject.FindWithTag("Player");    
        playerAnimation = Player.GetComponent<Animator>();
        PlayerMovement = Player.GetComponent <PlayerMovement>();
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(transform.position.x, transform.position.y, 5);
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 10){
            Vector2 location = new Vector2(transform.position.x, transform.position.y);
            Vector2 playerLocation = new Vector2(collision.gameObject.transform.position.x, collision.gameObject.transform.position.y);
            RaycastHit2D collisionInfo = Physics2D.Raycast(location, playerLocation - location);
            if (collisionInfo.transform != null && collisionInfo.transform.gameObject.layer == 10)
            {
                animator.SetTrigger("Enemy" + Side + "Slashing");
                playerAnimation.Play("PlayerDying");
                PlayerMovement.enabled = false;
                transform.position = Vector2.MoveTowards(transform.position, GameObject.FindWithTag("Player").transform.position, Time.deltaTime * speed);
                SceneManager.LoadScene(2);
            }
            else
            {
                animator.SetTrigger("Enemy" + Side + "Walking");
                transform.position = Vector2.MoveTowards(transform.position, GameObject.FindWithTag("Player").transform.position, Time.deltaTime * speed);
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 10)
        {
            animator.SetTrigger("EnemyDying");
        }
    }
}
