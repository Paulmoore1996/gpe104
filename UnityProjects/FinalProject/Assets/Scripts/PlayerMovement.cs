﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public GameObject player;//makes the player a variable
    private string direction;//creates a direction placeholder
    private Animator animator; //creates animation variable
    public float MovementSpeed;


	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();//get animation
        direction = "Back"; //preset direction 
	}
	
	// Update is called once per frame
	void Update () {

        // if a is pressed move left
        if (Input.GetKey(KeyCode.A))
        {
            direction = "Left";//Change direction to left
            animator.SetTrigger("PlayerLeftWalking");//play walking left animation
            transform.Translate(Vector2.left * Time.deltaTime * MovementSpeed);//move left
        }
        // if w is pressed move up
        if (Input.GetKey(KeyCode.W))
        {
            direction = "Back";//change direction to front
            animator.SetTrigger("PlayerBackWalking");//play walking back animation
            transform.Translate(Vector2.up * Time.deltaTime * MovementSpeed);//move up
        }
        // if d is pressed move right
        if (Input.GetKey(KeyCode.D))
        {
            direction = "Right";//change direction to right
            animator.SetTrigger("PlayerRightWalking");//play walking right animation
            transform.Translate(Vector2.right * Time.deltaTime * MovementSpeed);//move right
        }
        // if s is pressed move down
        if (Input.GetKey(KeyCode.S))
        {
            direction = "Front";//change direction to Front
            animator.SetTrigger("PlayerFrontWalking");//play walking down animation
            transform.Translate(Vector2.down * Time.deltaTime * MovementSpeed);//move down
        }
        // if space is pressed attack in the direction you are facting	
        if (Input.GetKeyDown(KeyCode.Space))
        {
                animator.SetTrigger("Player" + direction + "Slashing");//play the "Player" + direction + "Slashing
                //Destroy objects hit
        }  
	}
}
