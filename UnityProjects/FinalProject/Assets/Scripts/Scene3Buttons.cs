﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene3Buttons : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void QuitGame()//function quit game
    {
        Application.Quit();//exits the application
    }
    public void StartGame()//go to the next scene
    {
        SceneManager.LoadScene(1);//loads the "Game" scene.
    }
}
