﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public float EnemyDespawn;

    //create an array of spawn points
    public Transform[] Left;
    public Transform[] Right;
    public Transform[] Top;
    public Transform[] Bottom;

    //enemyVariables
    public GameObject TopEnemy;
    public GameObject LeftEnemy;
    public GameObject BottomEnemy;
    public GameObject RightEnemy;

    //enemy limit
    private float TopEnemyLimit;
    private float BottomEnemyLimit;
    private float LeftEnemyLimit;
    private float RightEnemyLimit;
    public float MaxRightEnemyLimit;
    public float MaxLeftEnemyLimit;
    public float MaxTopEnemyLimit;
    public float MaxBottomEnemyLimit;

    //This will create a singleton GameManager.
    public static GameManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this.gameObject);
            Debug.Log("warnings: A second game manager was detected and destroyed.");
        }
    }


    // Use this for initialization
    void Start () {
        InvokeRepeating("SpawnLeft", 10f, 5f);
        InvokeRepeating("SpawnTop", 10f, 5f);
        InvokeRepeating("SpawnBottom", 10f, 5f);
        InvokeRepeating("SpawnRight", 10f, 5f);
    }

    // Update is called once per frame
    void Update()
    {
    }
    void SpawnTop()
    {

            int spawnPointIndex = Random.Range(0, Top.Length);
            GameObject newTopEnemy = Instantiate(TopEnemy, Top[spawnPointIndex].position, Top[spawnPointIndex].rotation);
            Destroy(newTopEnemy, EnemyDespawn);

    }
    void SpawnLeft()
    {

            int spawnPointIndex = Random.Range(0, Left.Length);
            GameObject newLeftEnemy = Instantiate(LeftEnemy, Left[spawnPointIndex].position, Left[spawnPointIndex].rotation);
            Destroy(newLeftEnemy, EnemyDespawn);
 
    }
    void SpawnBottom()
    {

            int spawnPointIndex = Random.Range(0, Left.Length);
            GameObject newBottomEnemy = Instantiate(BottomEnemy, Bottom[spawnPointIndex].position, Bottom[spawnPointIndex].rotation);
            Destroy(newBottomEnemy, EnemyDespawn);

    }
    void SpawnRight()
    {

            int spawnPointIndex = Random.Range(0, Left.Length);
            GameObject newRightEnemy = Instantiate(RightEnemy, Right[spawnPointIndex].position, Right[spawnPointIndex].rotation);
            Destroy(newRightEnemy, EnemyDespawn);

    }
}

