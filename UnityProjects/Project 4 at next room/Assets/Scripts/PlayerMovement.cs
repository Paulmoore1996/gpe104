﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    private Animator animator;//Sets Animator value private.
    public float speed; //designer can set speed value.
    private string direction;
    public float soundLevel;
    public LayerMask myLayers;
    public float jumpForce;
    public Rigidbody2D rb;
    Scene currentScene;
    public Transform tf;
    public bool isGrounded;
    public GameObject wall;
    public float jumping;
    private float defaultJumping;
    // Use this for initialization
    void Start()
    {
        defaultJumping = jumping;
        tf = GetComponent<Transform>();
        direction = "Right";
        animator = GetComponent<Animator>();//Sets the Animator as the animator component.
    }

    // Update is called once per frame
    void Update()
        
    {
        currentScene = SceneManager.GetActiveScene();
        if (Input.GetKey(KeyCode.UpArrow))//if up key is pressed
        {

            direction = "Back";//changes direction to Back
            animator.SetTrigger("PlayerBackWalking");//trigger back slashing motion
            transform.Translate(Vector2.up * Time.deltaTime * speed);//move forward by designed set speed
        }
        if (Input.GetKey(KeyCode.DownArrow))//if up key is pressed
        {

            direction = "Front";//changes direciton to Front
            animator.SetTrigger("PlayerFrontWalking");//trigger back slashing motion
            transform.Translate(Vector2.down * Time.deltaTime * speed);//move forward by designed set speed
        }
        if (Input.GetKey(KeyCode.LeftArrow))//When the left key is pressed
        {

            direction = "Left";//changes direction to left
            animator.SetTrigger("PlayerLeftWalking");//start the left walking animation
            transform.Translate(Vector2.left * Time.deltaTime * speed);//move left
        }
        if (Input.GetKey(KeyCode.RightArrow))//when the right key is pressed
        {

            direction = "Right";//changes direction to right
            animator.SetTrigger("PlayerRightWalking");//start the right animation
            transform.Translate(Vector2.right * Time.deltaTime * speed);//moves right
        }
        if (currentScene.name == "Main Scene" && Input.GetKeyDown(KeyCode.Space))
        {
            FireNoise();
            animator.SetTrigger("Player" + direction + "Slashing");//animate the direction value slashing.
        }
        if (currentScene.name == "Project 4" && Input.GetKeyDown(KeyCode.Space) && (isGrounded == true || jumping != 0))
        {
            jumping--;
            animator.SetTrigger("Player" + direction + "Walking");
            rb.AddForce(Vector2.up * jumpForce);
            isGrounded = false;
        }
    }
    private void FireNoise()//makes noise ((Sound confuses me.))
    {
        Collider2D[] nearbyEnemies = Physics2D.OverlapCircleAll(new Vector2(transform.position.x, transform.position.y), soundLevel, myLayers);
        for (int i = 0; i < nearbyEnemies.Length; i++)
        {
            Enemy currentEnemy = nearbyEnemies[i].gameObject.GetComponent<Enemy>() as Enemy;
            if (currentEnemy != null)
                currentEnemy.listen(gameObject.transform);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
                isGrounded = true;
            jumping = defaultJumping;
            }
        }
    }
