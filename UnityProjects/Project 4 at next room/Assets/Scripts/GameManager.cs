﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    //makes the GameManager singleton
    public static GameManager instance;
    public int score = 0;

    //Multiple canvas variables here.
    public GameObject MainScreen;
    public GameObject AboutScreen;
    public GameObject ControlScreen;
    public GameObject gameOver;
    public GameObject lifeImage;
    public GameObject two;
    public GameObject three;

    //variables to start the game.
    public GameObject player;
    public GameObject playerMenu;
    public GameObject background;
    public GameObject enemy;
    public GameObject enemy1;
    public GameObject enemy2;
    public float Lives;
    private float defaultLives;
    public PlayerMovement moving;
    public Animator playerAnimation;

    //audio variables
    public AudioSource Audio;
    public AudioClip MenuAudio;
    public AudioClip PlayerDeath;

    void Awake() {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        } else
        {
            Destroy(this.gameObject);
            Debug.Log("warnings: A second game manager was detected and destroyed.");
        }
        MainScreen.SetActive(true);//turns on the main screen.
        defaultLives = Lives;//keeps desinger set lives remembered.
        Audio = GetComponentInParent<AudioSource>();
        Audio.clip = MenuAudio;
        Audio.Play();
    }
    //Main screen button functions
    public void exitGame()//exit game button
    {
        Application.Quit();//quit game.
    }
    public void about()//about button
    {
        MainScreen.SetActive(false);//turn main screen off.
        AboutScreen.SetActive(true);//turn about screen on.
    }
    public void controls()//Control button
    {
        MainScreen.SetActive(false);//turn main screen off.
        ControlScreen.SetActive(true);//turn controls screen on.
    }

    //return button for about screen
    public void goBackFromAbout()//return button
    {
        AboutScreen.SetActive(false);//sets about screen off
        MainScreen.SetActive(true);//sets main screen on.
    }

    //return button for control screen
    public void goBackFromControl()//return button
    {
        ControlScreen.SetActive(false);//turn control screen off
        MainScreen.SetActive(true);//turn main screen on.
    }
    //start the game.
    public void startGame()//start button
    {
        Audio.Stop();
        MainScreen.SetActive(false);//turns off the main screen
        player.SetActive(true);//turns on the player
        enemy.SetActive(true);//turns on the enemy
        enemy1.SetActive(true);
        enemy2.SetActive(true);
        background.SetActive(true);//turns on the background
        enemy.transform.position = new Vector3(-0.06f, 1.91f, 0);//resets the enemy incase of misplacement.
        enemy1.transform.position = new Vector3(5.85f, 1.93f, 0);
        enemy2.transform.position = new Vector3(-6.27f, 1.95f, 0);
        player.transform.position = new Vector3(-6.550626f, -0.01327041f, 0);//resets player incase of misplacement
        lifeImage.SetActive(true);//turns on all three life counters (designer should not set more than 3 as the limit.)
    }
    public void reloadGame()//reloads the game
    {
        playerMenu.SetActive(false);//turns off the playerMenu
        playerAnimation.Play("PlayerRightIdle");//resets player animation
        player.SetActive(false);//turns off the player
        moving.enabled = true;//turns on player movement
        enemy.SetActive(false);//turns off enemy
        enemy1.SetActive(false);
        enemy2.SetActive(false);
        enemy.transform.position = new Vector3(-0.06f, 1.91f, 0);//resets enemy position
        enemy1.transform.position = new Vector3(5.85f, 1.93f, 0);
        enemy2.transform.position = new Vector3(-6.27f, 1.95f, 0);
        player.transform.position = new Vector3(-6.550626f, -0.01327041f, 0);//resets player position
        MainScreen.SetActive(true);//turns on the main menu
        Audio.clip = MenuAudio;
        Audio.Play();
        background.SetActive(false);//turns off the background
        gameOver.SetActive(false);//turns off game over 
        lifeImage.SetActive(false);//turns off the life image
        two.SetActive(true);//turns off life image 2
        three.SetActive(true);//turns off lime image 3
        Lives = defaultLives;//lives returns to default state.
    }
    public void GameOver()//runs game over.
    {
        Audio.Stop();
        gameOver.SetActive(false);
        Lives--;
        if (Lives == 2)
        {
            three.SetActive(false);
        }
        else if (Lives == 1) { 
        two.SetActive(false);
    }
        if (Lives > 0)
        {
            TryAgain();
        }
        else if (Lives <= 0)
        {
            reloadGame();
        }
    }
    public void TryAgain()//incase game is not over.
    {
        enemy.transform.position = new Vector3(-0.06f, 1.91f, 0);
        enemy1.transform.position = new Vector3(5.85f, 1.93f, 0);
        enemy2.transform.position = new Vector3(-6.27f, 1.95f, 0);
        player.transform.position = new Vector3(-6.550626f, -0.01327041f, 0);
        moving.enabled = true;
        playerAnimation.Play("PlayerRightIdle");
    }
    public void Scene2() 
    {
        player.GetComponent<Rigidbody2D>().gravityScale = 1;
        DontDestroyOnLoad(player);
        player.transform.position = new Vector3(-6.550626f, -0.01327041f, 0);
        SceneManager.LoadScene(1);
    }
}

