﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float hearingRange;//this is a hearing distance
    private Animator animator;//this is to gain access to enemy animation
    public GameObject Player;//this is to gain access to the player
    public GameObject GameOver;//this is to gain access for game over.
    public Animator playerAnimation;//This is the animation variable for the player
    public float speed;//this is the speed the player will move
    public PlayerMovement moving;//this is to gain access to the moving script
    public GameObject MainCamera;

    public AudioSource Audio;
    public AudioClip PlayerDeath;
    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();//get the animation from the GameObject here
        Audio = MainCamera.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void listen(Transform location)
    {
#pragma warning disable CS0642 // Possible mistaken empty statement
        if (Mathf.Abs(Vector3.Distance(transform.position, transform.position)) < hearingRange);//THIS CONFUSES ME GREATLY BUT IT WORKS The ABS in specific is what is confusing. 
#pragma warning restore CS0642 // Possible mistaken empty statement
        animator.SetTrigger("EnemyFrontIdle");
    }
    private void OnTriggerStay2D(Collider2D collision)//if a collider enters this collider trigger
    {
        if (collision.gameObject.layer == 10)//and is on layer ten
        {
            Vector2 location = new Vector2(transform.position.x, transform.position.y);
            Vector2 playerLocation = new Vector2(collision.gameObject.transform.position.x, collision.gameObject.transform.position.y);
            RaycastHit2D collisionInfo = Physics2D.Raycast(location, playerLocation - location);
            if (collisionInfo.transform != null && collisionInfo.transform.gameObject.layer == 10)//and does not equal null, while being on layer ten
            {
                Audio.clip = PlayerDeath;
                Audio.Play();
                animator.SetTrigger("EnemyFrontSlashing");//Attack animation
                playerAnimation.Play("PlayerDying");//the player dies
                moving.enabled = false;//stop movement controls
                GameOver.SetActive(true);//activate game over function.
                transform.position = Vector2.MoveTowards(transform.position, Player.transform.position, Time.deltaTime * speed);//at a base speed set by designer

            }
            else
            {
                animator.SetTrigger("EnemyFrontWalking");//otherwise the enemy only walks 
                transform.position = Vector2.MoveTowards(transform.position, Player.transform.position, Time.deltaTime * speed);//at a base speed set by designer
            }
        }
        }
    private void OnTriggerExit2D(Collider2D collision)//if player leaves the enemy sight
    {
       if (collision.gameObject.layer == 10)//being on layer ten
        {
            animator.SetTrigger("EnemyDying");//reset enemy to dying state. 
        }
    }
}