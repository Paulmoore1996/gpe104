﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameMenu : MonoBehaviour
{
    public GameObject playerMenu;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))//if escape is pressed
        {
#pragma warning disable CS0618 // Type or member is obsolete
            if (playerMenu.active == false)
#pragma warning restore CS0618 // Type or member is obsolete
            {
                playerMenu.SetActive(true);//in game menu turns on.
            }
#pragma warning disable CS0618 // Type or member is obsolete
            else if (playerMenu.active == true)
#pragma warning restore CS0618 // Type or member is obsolete
            {
                playerMenu.SetActive(false);//disable playerMenu
            }
        }
    }
}
