﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioClips : MonoBehaviour {
    public AudioSource MainCamera;
    public AudioClip PlayerDeath;
    public AudioClip MenuMusic;

    public GameObject player;
	// Use this for initialization
	void Start () {
        MainCamera.GetComponent<AudioSource>();
        MainCamera.clip = MenuMusic;
        MainCamera.Play();
	}
	
	// Update is called once per frame
	void Update () {
             
	}
}
